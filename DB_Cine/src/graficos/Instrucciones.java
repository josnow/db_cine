package graficos;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.Font;
import java.awt.Window.Type;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;

public class Instrucciones extends JFrame {

	private JPanel contentPane;

	JTextArea Texto = new JTextArea();
	JScrollPane scrollPane = new JScrollPane();
	String BotonIngreso;

	public Instrucciones (String enviar) {

		BotonIngreso = enviar;

		setType(Type.UTILITY);
		setTitle("Instrucciones");
		setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		setBounds(100, 100, 450, 300);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);


		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setBounds(0, 0, 434, 261);
		contentPane.add(scrollPane);


		Texto.setFont(new Font("Dialog", Font.PLAIN, 13));
		Texto.setWrapStyleWord(true);
		Texto.setLineWrap(true);
		scrollPane.setViewportView(Texto);
		
		if (BotonIngreso == "VA") {
			InstruccionesVA();
		}
		if (BotonIngreso == "ES") {
			InstruccionesES();
		}
		if (BotonIngreso == "VF") {
			InstruccionesVF();
		}
		if (BotonIngreso == "VI") {
			InstruccionesVI();
		}
		if (BotonIngreso == "IS") {
			InstruccionesIS();
		}
		if (BotonIngreso == "PB") {
			InstruccionesPB();
		}
		if (BotonIngreso == "PC") {
			InstruccionesPC();
		}
		if (BotonIngreso == "RU") {
			InstruccionesRU();
		}
		Texto.setEditable(false);
	}	

	public void InstruccionesVA (){
		Texto.setText(
				"Administracion de usuarios: \r\n\r\nA continuacion podras editar y administrar los usuarios que hacen parte de la base de datos del cineUD, selecciona el usuario que quieras y eliminalo, si no deseas continuar puedes darle al boton 'Salir'.");
	}

	public void InstruccionesES (){
		Texto.setText(
				"Reservar Boleta: \r\n\r\nA continuacion podras escoger la sillas que esten disponibles en el horario escogido, simplemente marca en la silla que quieras sentarte y reserva tu boleta. Si no deseas continuar dale al boton 'atras'.");
	}

	public void  InstruccionesVF () {
		Texto.setText(
				"Selecciona la funcion: \r\n\r\nEn esta ventana podras escoger entre las funciones que estan la que mas te guste, simplemente selecciona la funcion de la tabla haciendo click en esta y luego dale al boton escoger asiento (no olvides seleccionar anteriormente la funcion de la tabla).");
	}

	public void  InstruccionesVI () {
		Texto.setText(
				"Bienvenido al Cine UDISTRITAL: \r\n\r\nEn esta ventana podras escoger entre 3 de las siguientes opciones. \r\n\r\nPeliculas en cartelera: Si seleccionas esta opcion podras escoger entre las funciones disponibles, hacer una reserva y disfrutar al maximo el dia. \r\n\r\nRegistrarse: Si oprimes este boton accederas a la ventana de registrarse, si te registras accederas a la posibilidad de pagar la boleta mediante este aplicativo. Sino te registras podras ver las peliculas en cartelera y las funciones disponibles. \r\n\r\nIniciar Sesion: En esta ventana podras acceder a tu cuenta y podras reservar tu boleta. Si no tienes una cuenta, registrate.");
	}

	public void InstruccionesIS () {
		Texto.setText(
				"Inicion de sesion: \r\n\r\nEn esta ventana podras iniciar sesion con el numero de documento registrado previamente, luego de ingresarlo oprime el boton 'Ingresar'. Si no deseas ingresar sesion puedes darle al boton 'atras'.");
	}

	public void InstruccionesPB () {
		Texto.setText(
				"Pagar boleta: \r\n\r\nEn esta vetana podras pagar tus boletas reservadas. En el panel izquierdo estaran los datos de las funciones que escogiste, en el central tendras la opcion de ingresar el numero de tarjeta de credito para pagar tus boletas, ademas de escoger el tipo de tarjeta con la que realizaras el pago y en el panel derecho tendras el boton 'pagar'.\r\n\r\n�No olvides ingresar el numero de tu tarjeta y escoger el tipo de tarjeta con la cual vas a realizar el pago antes de darle al boton!.\r\n\r\n");
	}

	public void InstruccionesPC () {
		Texto.setText(
				"Peliculas en cartelera: \r\n\r\nEn esta ventana podras escoger la pelicula que desees. Primero escoge la pelicula dentro de la tabla dandole click a la funcion y luego dale al boton 'reservar' (debes iniciar sesion previamente), luego accederas a otra pesta�a para reservar la funcion. Si no deseas continuar con el pago de boletas, podras devolverte dandole al boton 'atras'. ");
	}

	public void InstruccionesRU () {
		Texto.setText(
				"Registro de usuario:\r\n\r\nBienvenido a la pesta�a 'registro de usuario' para completar la creacion de tu cuenta diligencia tus datos en orden, tal y como te lo pide el programa, luego de haber completado todos los datos, oprime el boton 'registrar'. Al registrarte podras reservar boletas mediante el pago en tarjetas credito / debito. Si no deseas continuar con la creacion de tu cuenta dale al boton 'atras' ");
	}
}
