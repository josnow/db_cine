package graficos;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class VistaAdmin extends JFrame {
	JTable tablaUsuarios;
	DefaultTableModel modeloTabla;
	JButton btnEliminar;
	JButton btnSalir;
	private Connection conexion;
	String Enviar;
	private JToolBar toolBar;
	private JButton btnInstrucciones;
	
	

	public VistaAdmin(Connection conexion) {
		Enviar = "VA";
		this.conexion = conexion;
		
		
		
		
		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension tamanioPantalla = miPantalla.getScreenSize();

		int alturaPantalla = tamanioPantalla.height;
		int anchoPantalla = tamanioPantalla.width;

		setSize((int) (anchoPantalla / 1.2), (int) (alturaPantalla / 3));
		setLocation((int) (anchoPantalla / 12), alturaPantalla / 4);
		setTitle("Administracion de usuarios");
		setLayout(new BorderLayout());

		// A�adir icono ----------------------------------------------------
		Image miIcono;

		try {
			miIcono = ImageIO.read(new File("src/imagenes/icono1.png"));
			setIconImage(miIcono);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// -----------------------------------------------------------------

		JLabel lblTitulo = new JLabel("Administrar");
		JPanel lamSup = new JPanel();
		lamSup.add(lblTitulo);
		add(lamSup, BorderLayout.NORTH);

		tablaUsuarios = new JTable();
		modeloTabla = new DefaultTableModel();
		tablaUsuarios.setModel(modeloTabla);
		add(new JScrollPane(tablaUsuarios));
		
		JPanel lamBotones = new JPanel();
		btnEliminar = new JButton("Eliminar");
		btnEliminar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
				// Ejecutar delete de la tabla usuario  --------------------------------

				String consulta = "DELETE FROM Usuario WHERE id_usuario="+tablaUsuarios.getValueAt(tablaUsuarios.getSelectedRow(), 0);
				Statement sentencia = null;
				try {
					sentencia = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
		            
		            sentencia.executeUpdate(consulta);
		            System.out.println("Delete exitoso");
		            iniciar();
		            JOptionPane.showMessageDialog(getVisAd(),
							"Eliminacion exitosa", "Administrando",
							JOptionPane.INFORMATION_MESSAGE);
					sentencia.close();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}

				// -------------------------------------------------------------------------
				
				
			}
			
			
		});
		
		toolBar = new JToolBar();
		lamBotones.add(toolBar);
		toolBar.setOrientation(SwingConstants.VERTICAL);
		toolBar.setLocation(150, 20);
		
		btnInstrucciones = new JButton("Instrucciones");
		toolBar.add(btnInstrucciones);
		btnInstrucciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.out.println("asd");
				Instrucciones I = new Instrucciones(Enviar);
				I.setVisible(true);
				I.setResizable(false);
;			}
		});
		
		lamBotones.add(btnEliminar);
		
		btnSalir = new JButton("Salir");
		btnSalir.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				
				System.exit(0);
			}
			
		});
		
		lamBotones.add(btnSalir);
		add(lamBotones, BorderLayout.SOUTH);
	}

	public void iniciar() {

		modeloTabla.setColumnCount(0);
		modeloTabla.setRowCount(0);
		modeloTabla.addColumn("Id usuario");
		modeloTabla.addColumn("Tipo usuario");
		modeloTabla.addColumn("Primer Nombre");
		modeloTabla.addColumn("Segundo Nombre");
		modeloTabla.addColumn("Primer Apellido");
		modeloTabla.addColumn("Segundo Apellido");
		modeloTabla.addColumn("Fecha de nacimiento");
		modeloTabla.addColumn("Correo");
		modeloTabla.addColumn("Numero de documento");

		// Ejecutar consulta de la tabla usuario --------------------------------

		ResultSet rsRegistros = null;
		String consulta = "SELECT * FROM Usuario";
		Statement sentencia = null;
		try {
			sentencia = conexion.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rsRegistros = sentencia.executeQuery(consulta);
			while (rsRegistros.next()) {

				String[] fila = new String[9];

				fila[0] = rsRegistros.getString("Id_Usuario");
				if(rsRegistros.getString("Ti_id_tipo_usu").equals("1"))
					fila[1] = "Normal";
				else if(rsRegistros.getString("Ti_id_tipo_usu").equals("2"))
					fila[1] = "Platino";
				else if(rsRegistros.getString("Ti_id_tipo_usu").equals("3"))
					fila[1] = "Pluss";
				
				fila[2] = rsRegistros.getString("Primer_Nombre");
				fila[3] = rsRegistros.getString("Segundo_Nombre");
				fila[4] = rsRegistros.getString("Primer_Apellido");
				fila[5] = rsRegistros.getString("Segundo_Apellido");
				fila[6] = rsRegistros.getString("Fecha_Nacimiento");
				fila[7] = rsRegistros.getString("Correo_Usuario");
				fila[8] = rsRegistros.getString("Numero_Documento");
				
				modeloTabla.addRow(fila);
			}
			sentencia.close();
			
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// -------------------------------------------------------------------------

		setVisible(true);
	}
	
	public VistaAdmin getVisAd() {
		return this;
	}
}
