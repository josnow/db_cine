package graficos;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.JToolBar;

public class VistaInicioSesion extends JFrame {

	private JButton btnAtras;
	private JButton btnIngresar;
	private VistaInicial visIni;
	String Enviar;
	private JToolBar toolBar;
	private JButton btnInstrucciones;

	public VistaInicioSesion(VistaInicial visIni) {
		Enviar = "IS";
		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension tamanioPantalla = miPantalla.getScreenSize();

		int alturaPantalla = tamanioPantalla.height;
		int anchoPantalla = tamanioPantalla.width;

		setSize(anchoPantalla / 3, alturaPantalla / 5);
		setLocation(anchoPantalla / 3, alturaPantalla / 4);
		setTitle("Iniciar sesion");
		setLayout(new BorderLayout(10, 10));

		// A�adir icono ----------------------------------------------------
		Image miIcono;

		try {
			miIcono = ImageIO.read(new File("src/imagenes/icono1.png"));
			setIconImage(miIcono);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// -----------------------------------------------------------------
		this.visIni = visIni;
		
		JPanel laminaNorte = new JPanel(); // Lamina superior
		JLabel lblMenTitulo = new JLabel("Iniciar Sesion");

		laminaNorte.add(lblMenTitulo);

		JPanel laminaCentro = new JPanel(); // Lamina central
		JLabel lblMenNumDoc = new JLabel("Numero documento:");
		JTextField txtNumDoc = new JTextField(15);

		laminaCentro.add(lblMenNumDoc);
		laminaCentro.add(txtNumDoc);

		JPanel laminaSur = new JPanel();

		btnAtras = new JButton("Atras");
		btnAtras.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {

				getVistaIniSe().setVisible(false);
				visIni.iniciar();
			}
		});


		JToolBar toolBar = new JToolBar();
		laminaSur.add(toolBar);
		
		JButton btnInstrucciones = new JButton("Instrucciones");
		toolBar.add(btnInstrucciones);
		btnInstrucciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Instrucciones I = new Instrucciones(Enviar);
				I.setVisible(true);
				I.setResizable(false);
			}
		});

		btnIngresar = new JButton("Ingresar");
		btnIngresar.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				// Ejecutar consulta de la tabla usuario --------------------------------

				ResultSet rsRegistros;
				String consulta = "SELECT * FROM usuario";
				Statement sentencia;
				try {
					sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
					rsRegistros = sentencia.executeQuery(consulta);
					boolean aux=true;
					while (rsRegistros.next()) {
						if(rsRegistros.getString("numero_documento").equals(txtNumDoc.getText())) {
							getVistaIniSe().setVisible(false);
							visIni.setId_usuario(Integer.parseInt(rsRegistros.getString("id_usuario")));
							visIni.getLblNombre().setText(rsRegistros.getString("primer_nombre"));
							JOptionPane.showMessageDialog(getVistaIniSe(), "Bienvenido: "+rsRegistros.getString("primer_nombre"), 
									"Ingreso Exitoso", JOptionPane.INFORMATION_MESSAGE);
							visIni.getBtnIniSes().setEnabled(false);
					        visIni.getBtnRegUsu().setEnabled(false);
							aux=false;
							visIni.setVisible(true);
						}
					}
					if(aux)
						JOptionPane.showMessageDialog(getVistaIniSe(), "Verifique su documento por favor", 
							"Usuario no encontrado", JOptionPane.INFORMATION_MESSAGE);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

				// -------------------------------------------------------------------------

			}
		});

		laminaSur.add(btnAtras);
		laminaSur.add(btnIngresar);

		add(laminaNorte, BorderLayout.NORTH);
		add(laminaCentro, BorderLayout.CENTER);
		add(laminaSur, BorderLayout.SOUTH);
	}

	public void iniciar() {

		setVisible(true);
	}

	private VistaInicioSesion getVistaIniSe() {
		return this;
	}
}
