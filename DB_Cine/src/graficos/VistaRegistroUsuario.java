package graficos;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.*;
import javax.imageio.ImageIO;

public class VistaRegistroUsuario extends JFrame {

	private JButton btnAtras;
	private JButton btnRegistrar;
	private VistaInicial visIni;
	private JComboBox tipoUsuario;
	JPanel laminaNorte;
	JLabel lblMenTitulo;
	JPanel laminaCentral;
	JLabel lblMnsTipoUsuario;
	JPanel laminaSur;
	String Enviar;
	private JToolBar toolBar;
	private JButton btnInstrucciones;
	
	public VistaRegistroUsuario(VistaInicial visIni) {
		Enviar="RU";
		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension tamanioPantalla = miPantalla.getScreenSize();

		int alturaPantalla = tamanioPantalla.height;
		int anchoPantalla = tamanioPantalla.width;

		setSize(anchoPantalla / 3, alturaPantalla / 2 );
		setLocation(anchoPantalla / 3, alturaPantalla / 4);
		setTitle("Registro de usuario");
		setLayout(new BorderLayout(10, 10));

		// A�adir icono ----------------------------------------------------
		Image miIcono;

		try {
			miIcono = ImageIO.read(new File("src/imagenes/icono1.png"));
			setIconImage(miIcono);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// -----------------------------------------------------------------

		this.visIni = visIni;
		
		laminaNorte = new JPanel(); // Lamina superior
		
		// Formulario registro ---------------------------------------------
		
		lblMenTitulo = new JLabel("Ingrese sus datos:");
		laminaNorte.add(lblMenTitulo);
		laminaCentral = new JPanel(); // Lamina central
		laminaCentral.setLayout(new GridLayout(8, 2, 20, 10));
		lblMnsTipoUsuario = new JLabel("Tipo de usuario: ");
		laminaCentral.add(lblMnsTipoUsuario);
		tipoUsuario = new JComboBox();
		tipoUsuario.addItem("Normal");
		tipoUsuario.addItem("Platino");
		tipoUsuario.addItem("Pluss");
		
		laminaCentral.add(tipoUsuario);
		
		JLabel lblMnsPrimerNombre = new JLabel("Primer nombre: ");
		laminaCentral.add(lblMnsPrimerNombre);
		
		JTextField txtPrimerNombre = new JTextField();
		laminaCentral.add(txtPrimerNombre);
		
		JLabel lblMnsSegundoNombre = new JLabel("Segundo nombre: ");
		laminaCentral.add(lblMnsSegundoNombre);
		
		JTextField txtSegundoNombre = new JTextField();
		laminaCentral.add(txtSegundoNombre);
		
		JLabel lblMnsPrimerApellido = new JLabel("Primer apellido: ");
		laminaCentral.add(lblMnsPrimerApellido);
		
		JTextField txtPrimerApellido = new JTextField();
		laminaCentral.add(txtPrimerApellido);
		
		JLabel lblMnsSegundoApellido = new JLabel("Segundo apellido: ");
		laminaCentral.add(lblMnsSegundoApellido);
		
		JTextField txtSegundoApellido = new JTextField();
		laminaCentral.add(txtSegundoApellido);
		
		JLabel lblMnsFechaNacimiento = new JLabel("Fecha de nacimiento: ");
		laminaCentral.add(lblMnsFechaNacimiento);
		
		JTextField txtFechaNacimiento = new JTextField();
		laminaCentral.add(txtFechaNacimiento);
		
		JLabel lblMnsCorreo = new JLabel("Correo electronico: ");
		laminaCentral.add(lblMnsCorreo);
		
		JTextField txtCorreo = new JTextField();
		laminaCentral.add(txtCorreo);
		
		JLabel lblMnsNumDoc= new JLabel("Numero de documento: ");
		laminaCentral.add(lblMnsNumDoc);
		
		JTextField txtNumDoc = new JTextField();
		laminaCentral.add(txtNumDoc);
		JToolBar toolBar = new JToolBar();
		laminaSur.add(toolBar);
		
		JButton btnInstrucciones = new JButton("Instrucciones");
		toolBar.add(btnInstrucciones);
		btnInstrucciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Instrucciones I = new Instrucciones(Enviar);
				I.setVisible(true);
				I.setResizable(false);
			}
		});
		
		
		// Fin formulario registro --------------------------------------------
		
		laminaSur = new JPanel(); // Lamina de botones -----------------
		btnAtras = new JButton("Atras");
		btnAtras.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				getVisReg().setVisible(false);
				visIni.iniciar();
			}
		});
		
		btnRegistrar = new JButton("Registrarse");
		btnRegistrar.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				int tipoUsu = tipoUsuario.getSelectedIndex()+1;
				String primerNombre = txtPrimerNombre.getText();
				String segundoNombre = txtSegundoNombre.getText();
				String primerApellido= txtPrimerApellido.getText();
				String segundoApellido = txtSegundoApellido.getText();
				String fechaNacimiento = txtFechaNacimiento.getText();
				String correo = txtCorreo.getText();
				int numeroDocumento = Integer.parseInt(txtNumDoc.getText());
				
				String sql = "INSERT INTO Usuario (Ti_id_tipo_usu, Primer_Nombre, Segundo_Nombre, Primer_Apellido, Segundo_Apellido, Fecha_Nacimiento, Correo_Usuario, Numero_Documento) "
						+ "VALUES ("+tipoUsu+", '"+primerNombre+"', '"+segundoNombre+"', '"+primerApellido+"', '"+segundoApellido+"', '"+fechaNacimiento+"', '"+correo+"', "+numeroDocumento+")";
				
				try {
					Statement sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
					
					sentencia.executeUpdate(sql);
					
					// Cerramos las conexiones, en orden inverso a su apertura
					sentencia.close();
					String consulta = "SELECT * FROM usuario";
					sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
							ResultSet.CONCUR_READ_ONLY);
					ResultSet rsRegistros = sentencia.executeQuery(consulta);
					while (rsRegistros.next()) {
						if(rsRegistros.getString("numero_documento").equals(String.valueOf(numeroDocumento))) {
							visIni.setId_usuario(Integer.parseInt(rsRegistros.getString("id_usuario")));
						}
					}
					sentencia.close();
					System.out.println(visIni.getId_usuario());
					JOptionPane.showMessageDialog(visIni, "Usuario registrado exitosamente", "Felicidades", JOptionPane.INFORMATION_MESSAGE);
					visIni.getLblNombre().setText(primerNombre);
					visIni.getBtnIniSes().setEnabled(false);
					visIni.getBtnRegUsu().setEnabled(false);
				} catch (SQLException e1) {
					JOptionPane.showMessageDialog(getVisReg(), "No se pudo registrar el usuario", "Error ", JOptionPane.INFORMATION_MESSAGE);
					e1.printStackTrace();
				}
				getVisReg().setVisible(false);
				
				visIni.setVisible(true);
			}
		});
		
		laminaSur.add(btnAtras);
		laminaSur.add(btnRegistrar);
		
		//---------------------------------------------------------------------
		JPanel laminaOeste = new JPanel(); // Para a�adir un espaciado al oeste del formulario
		JPanel laminaEste = new JPanel(); // Para a�adir un espaciado al este del formulario
		
		
		add(laminaNorte, BorderLayout.NORTH);
		add(laminaCentral, BorderLayout.CENTER);
		add(laminaSur, BorderLayout.SOUTH);
		add(laminaOeste, BorderLayout.WEST);
		add(laminaEste, BorderLayout.EAST);
		
	}
	
	public void iniciar() {				
		
		setVisible(true);
	}
	
	public VistaRegistroUsuario getVisReg() {
		return this;
	}
}
