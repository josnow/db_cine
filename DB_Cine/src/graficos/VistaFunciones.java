package graficos;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.table.DefaultTableModel;

public class VistaFunciones extends JFrame {

	private VistaInicial visIni;
	JLabel lblTitulo;
	JPanel laminaSuperior;
	JPanel laminaCentral;
	JPanel laminaInferior;
	JTable tablaFunciones;
	JButton btnAtras;
	JButton btnEscojerAsiento;
	DefaultTableModel modeloTabla;
	String Enviar;
	private JToolBar toolBar;
	private JButton btnInstrucciones;

	public VistaFunciones(VistaInicial visIni) {
		Enviar = "VF";
		setLayout(new BorderLayout());

		this.visIni = visIni;

		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension tamanioPantalla = miPantalla.getScreenSize();

		int alturaPantalla = tamanioPantalla.height;
		int anchoPantalla = tamanioPantalla.width;

		setSize((int) (anchoPantalla / 1.2), (int) (alturaPantalla / 3));
		setLocation((int) (anchoPantalla / 12), alturaPantalla / 5);
		setTitle("Seleccionar funcion");

		// A�adir icono ----------------------------------------------------
		Image miIcono;

		try {
			miIcono = ImageIO.read(new File("src/imagenes/icono1.png"));
			setIconImage(miIcono);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// -----------------------------------------------------------------

		laminaSuperior = new JPanel();
		laminaCentral = new JPanel();
		laminaInferior = new JPanel();
		laminaCentral.setLayout(new BorderLayout());
		laminaSuperior.setLayout(new BorderLayout());

		lblTitulo = new JLabel("Escoge una funcion: ");
		laminaSuperior.add(lblTitulo, BorderLayout.NORTH);
		
		tablaFunciones = new JTable();
		modeloTabla = new DefaultTableModel();
		tablaFunciones.setModel(modeloTabla);
		laminaCentral.add(new JScrollPane(tablaFunciones));
		
		// Botones 
		
		btnAtras = new JButton("Atras");
		btnAtras.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				getVisFun().setVisible(false);
				visIni.getVisPelCar().iniciar();
			}
			
		});
		toolBar = new JToolBar();
		laminaInferior.add(toolBar);
		
		btnInstrucciones = new JButton("Instrucciones");
		toolBar.add(btnInstrucciones);
		btnInstrucciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Instrucciones I = new Instrucciones(Enviar);
				I.setVisible(true);
				I.setResizable(false);
			}
		});
		laminaInferior.add(btnAtras);
		
		btnEscojerAsiento = new JButton("Escojer asiento");
		btnEscojerAsiento.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent arg0) {
				visIni.getVisEsSi().iniciar(Integer.parseInt((String) tablaFunciones.getValueAt(tablaFunciones.getSelectedRow(), 0)));
				getVisFun().setVisible(false);
			}
			
		});
		laminaInferior.add(btnEscojerAsiento);
		
		
		add(laminaSuperior, BorderLayout.NORTH);
		add(laminaCentral, BorderLayout.CENTER);
		add(laminaInferior, BorderLayout.SOUTH);
	}

	public void iniciar(int id_pelicula) {

		modeloTabla.setColumnCount(0);
		modeloTabla.setRowCount(0);
		modeloTabla.addColumn("Numero funcion");
		modeloTabla.addColumn("Dia");
		modeloTabla.addColumn("Jornada");
		modeloTabla.addColumn("Sala");

		// Ejecutar consulta de la tabla pelicula --------------------------------

		ResultSet rsRegistros = null;
		String consulta = "SELECT Dia_id_dia, Jor_id_jornada, Sal_id_sala, id_funcion FROM funcion WHERE Pel_id_peli="+id_pelicula;
		Statement sentencia = null;
		try {
			sentencia = visIni.getConexion().createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE,
					ResultSet.CONCUR_READ_ONLY);
			rsRegistros = sentencia.executeQuery(consulta);
			while (rsRegistros.next()) {

				String[] fila = new String[4];
				
				fila[0] = rsRegistros.getString("id_funcion");
				if(rsRegistros.getString("Dia_id_dia").equals("1")) {
					fila[1] = "Lunes";	
				}else if(rsRegistros.getString("Dia_id_dia").equals("2")) {
					fila[1] = "Martes";	
				}else if(rsRegistros.getString("Dia_id_dia").equals("3")) {
					fila[1] = "Miercoles";	
				}else if(rsRegistros.getString("Dia_id_dia").equals("4")) {
					fila[1] = "Jueves";	
				}else if(rsRegistros.getString("Dia_id_dia").equals("5")) {
					fila[1] = "Viernes";	
				}else if(rsRegistros.getString("Dia_id_dia").equals("6")) {
					fila[1] = "Sabado";	
				}else if(rsRegistros.getString("Dia_id_dia").equals("7")) {
					fila[1] = "Domingo";	
				}
				
				if(rsRegistros.getString("Jor_id_jornada").equals("1")) {
					fila[2] = "Ma�ana";	
				}else if(rsRegistros.getString("Jor_id_jornada").equals("2")) {
					fila[2] = "Tarde";	
				}else if(rsRegistros.getString("Jor_id_jornada").equals("3")) {
					fila[2] = "Noche";	
				}
				
				if(rsRegistros.getString("Sal_id_sala").equals("1")) {
					fila[3] = "Sala Norte 01";	
				}else if(rsRegistros.getString("Sal_id_sala").equals("2")) {
					fila[3] = "Sala Sur 02";	
				}
				
				modeloTabla.addRow(fila);
			}
			sentencia.close();
		} catch (SQLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		// -------------------------------------------------------------------------
		setVisible(true);

	}

	public VistaInicial getVisIni() {
		return visIni;
	}

	public VistaFunciones getVisFun() {
		return this;
	}
}
