package graficos;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.sql.*;
import javax.imageio.ImageIO;
import javax.swing.*;

public class VistaInicial extends JFrame {

	private JButton btnPelCar;
	private JButton btnIniSes;
	private JButton btnRegUsu;
	private VistaPeliculasCartelera visPelCar;
	private VistaInicioSesion visInSe;
	private VistaRegistroUsuario visReg;
	private VistaEscojerSillas visEsSi;
	private DatabaseMetaData mDatosBD;
	private Connection conexion;
	private VistaPagarBoletos visPagBol;
	private VistaFunciones visFun;
	private int id_usuario;
	
	private JPanel laminaPrincipal;
	private JLabel lblMenBien;
	private JPanel laminaCentral;
	private JLabel lblSelecOp;
	JPanel laminaInferior;
	JLabel lblNombre;
	String Enviar;
	private JToolBar toolBar;
	private JButton btnInstrucciones;

	public VistaInicial(Connection conexion) {
		Enviar = "VI";
		this.id_usuario = 0;
		this.conexion = conexion;

		try {
			mDatosBD = conexion.getMetaData();
		} catch (SQLException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}

		Toolkit miPantalla = Toolkit.getDefaultToolkit();
		Dimension tamanioPantalla = miPantalla.getScreenSize();

		int alturaPantalla = tamanioPantalla.height;
		int anchoPantalla = tamanioPantalla.width;

		setSize(anchoPantalla / 3, alturaPantalla / 5);
		setLocation(anchoPantalla / 3, alturaPantalla / 4);
		setTitle("Cine UDistrital");

		// A�adir icono ----------------------------------------------------
		Image miIcono;

		try {
			miIcono = ImageIO.read(new File("src/imagenes/icono1.png"));
			setIconImage(miIcono);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// -----------------------------------------------------------------
		laminaPrincipal = new JPanel(); // Lamina superior
		laminaCentral = new JPanel(); // Lamina central
		laminaInferior = new JPanel(); // Lamina inferior
		lblMenBien = new JLabel();
		lblSelecOp = new JLabel();
		lblNombre = new JLabel();
		
		// Inicializar botones
		
		btnPelCar = new JButton("Peliculas en cartelera");
		btnPelCar.addActionListener(new ActionListener() { // evento del boton Peliculas en cartelera
			
			@Override
			public void actionPerformed(ActionEvent e) {
				
				getVistaInicial().setVisible(false);
				getVisPelCar().iniciar();
			}
		});
		toolBar = new JToolBar();
		laminaInferior.add(toolBar);
		
		btnInstrucciones = new JButton("Instrucciones");
		toolBar.add(btnInstrucciones);
		btnInstrucciones.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Instrucciones I = new Instrucciones(Enviar);
				I.setVisible(true);
				I.setResizable(false);
			}
		});
		btnIniSes = new JButton("Iniciar Sesion");
		btnIniSes.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e1) {
				
				getVistaInicial().setVisible(false);
				getVisInSe().iniciar();
			}
			
		});
		
		btnRegUsu = new JButton("Registrarse");
		btnRegUsu.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e2) {
				
				getVistaInicial().setVisible(false);
				getVisReg().iniciar();
			}
		});
		
		// ----------------------------------------------
		
		// Titulo vista
				lblMenBien.setText("Bienvenido a Cine UDistrital");
				laminaPrincipal.add(lblMenBien);
				lblSelecOp.setText("Seleccione una opcion:");
				// Instruccion
				laminaCentral.add(lblSelecOp);
				laminaCentral.add(lblNombre);

				laminaInferior.add(btnPelCar);
				laminaInferior.add(btnIniSes);
				laminaInferior.add(btnRegUsu);
				//setLayout(new BorderLayout(0, 10));
				add(laminaPrincipal, BorderLayout.NORTH);
				add(laminaCentral, BorderLayout.CENTER);
				add(laminaInferior, BorderLayout.SOUTH);
	}

	public void iniciar() {
		setVisible(true);
	}

	public VistaPeliculasCartelera getVisPelCar() {

		if (visPelCar == null) {
			visPelCar = new VistaPeliculasCartelera(this);
			visPelCar.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}

		return visPelCar;
	}

	public VistaInicioSesion getVisInSe() {

		if (visInSe == null) {
			visInSe = new VistaInicioSesion(this);
			visInSe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		return visInSe;
	}

	public VistaRegistroUsuario getVisReg() {
		if (visReg == null) {
			visReg = new VistaRegistroUsuario(this);
			visReg.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		return visReg;
	}

	public VistaEscojerSillas getVisEsSi() {
		if (visEsSi == null) {
			visEsSi = new VistaEscojerSillas(this);
			visEsSi.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		return visEsSi;
	}

	public VistaPagarBoletos getVisPagBol() {
		if (visPagBol == null) {
			visPagBol = new VistaPagarBoletos(this);
			visPagBol.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		return visPagBol;
	}
	
	public VistaFunciones getVisFun() {
		if (visFun == null) {
			visFun = new VistaFunciones(this);
			visFun.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		}
		return visFun;
	}

	public VistaInicial getVistaInicial() {
		return this;
	}

	public DatabaseMetaData getmDatosBD() {
		return mDatosBD;
	}

	public Connection getConexion() {
		return conexion;
	}

	public JButton getBtnIniSes() {
		return btnIniSes;
	}

	public JButton getBtnRegUsu() {
		return btnRegUsu;
	}

	public void setBtnIniSes(JButton btnIniSes) {
		this.btnIniSes = btnIniSes;
	}

	public void setBtnRegUsu(JButton btnRegUsu) {
		this.btnRegUsu = btnRegUsu;
	}

	public int getId_usuario() {
		return id_usuario;
	}

	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}

	public JLabel getLblNombre() {
		return lblNombre;
	}

}
